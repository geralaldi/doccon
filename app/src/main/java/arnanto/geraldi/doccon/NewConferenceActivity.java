package arnanto.geraldi.doccon;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import arnanto.geraldi.doccon.model.Conference;

public class NewConferenceActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {

	EditText mConferenceNameField, mStartDateField, mStartTimeField, mEndDateField, mEndTimeField,
			mConferenceLocationField;

	TimePickerDialog startTimePickerDialog, endTimePickerDialog;
	Date startDate, endDate;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_new_conference);
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		setupView();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu_new_conference, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();

		//noinspection SimplifiableIfStatement
		if (id == R.id.action_save) {
			String name = mConferenceNameField.getText().toString();
			String location = mConferenceLocationField.getText().toString();
			if (validateInput(name, location, startDate, endDate)) {
				saveConference(name, location, startDate, endDate);
			}
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
		String tag = view.getTag();
		String dateString = formatDate(year, monthOfYear, dayOfMonth);
		if (tag.equals("StartDate")) {
			Date date = null;
			try {
				date = new SimpleDateFormat("yyyy-mm-dd").parse(year + "-" + monthOfYear + "-" + dayOfMonth);
				startDate = date;
			} catch (ParseException e) {
				e.printStackTrace();
			}
			mStartDateField.setText(dateString);
		} else if (tag.equals("EndDate")) {
			Date date = null;
			try {
				date = new SimpleDateFormat("yyyy-mm-dd").parse(year + "-" + monthOfYear + "-" + dayOfMonth);
				endDate = date;
			} catch (ParseException e) {
				e.printStackTrace();
			}
			mEndDateField.setText(dateString);
		}

		Log.i("TEST", tag);
	}

	@Override
	public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute) {
		// Still Error
		String tag = view.getTag().toString();

		String time = formatTime(hourOfDay, minute);
		if (tag.equals("StartTime")) {
			mStartTimeField.setText(time);
		} else if (tag.equals("EndTime")) {
			mEndTimeField.setText(time);
		}
	}

	private void setupView() {
		mConferenceNameField = (EditText) findViewById(R.id.field_conference_name);

		mStartDateField = (EditText) findViewById(R.id.field_start_date);
		mStartDateField.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Calendar now = Calendar.getInstance();
				DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(NewConferenceActivity.this, now.get
								(Calendar.YEAR), now.get(Calendar.MONTH),
						now.get(Calendar.DAY_OF_MONTH));
				datePickerDialog.show(getFragmentManager(), "StartDate");
			}
		});

		mStartTimeField = (EditText) findViewById(R.id.field_start_time);
		mStartTimeField.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Calendar now = Calendar.getInstance();
				startTimePickerDialog = TimePickerDialog.newInstance(NewConferenceActivity.this, now.get
						(Calendar.HOUR_OF_DAY), now.get(Calendar.MINUTE), true);
				startTimePickerDialog.show(getFragmentManager(), "StartTime");
			}
		});

		mEndDateField = (EditText) findViewById(R.id.field_end_date);
		mEndDateField.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Calendar now = Calendar.getInstance();
				DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(NewConferenceActivity.this, now.get
								(Calendar.YEAR), now.get(Calendar.MONTH),
						now.get(Calendar.DAY_OF_MONTH));
				datePickerDialog.show(getFragmentManager(), "EndDate");
			}
		});

		mEndTimeField = (EditText) findViewById(R.id.field_end_time);
		mEndTimeField.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Calendar now = Calendar.getInstance();
				endTimePickerDialog = TimePickerDialog.newInstance(NewConferenceActivity.this, now.get
						(Calendar.HOUR_OF_DAY), now.get(Calendar.MINUTE), true);
				endTimePickerDialog.show(getFragmentManager(), "EndTime");
			}
		});

		mConferenceLocationField = (EditText) findViewById(R.id.field_conference_location);
	}

	private String formatDate(int year, int monthOfYear, int dayOfMonth) {
		String dateString = "";
		Calendar now = Calendar.getInstance();
		Calendar then = Calendar.getInstance();
		try {
			Date date = new SimpleDateFormat("yyyy-mm-dd").parse(year + "-" + monthOfYear + "-" + dayOfMonth);
			then.setTime(date);

			if (now.before(then)) {
				// Show date error
			} else if (now.equals(then)) {
				dateString = "Today";
			} else {
				dateString = new SimpleDateFormat("E, MMM dd").format(date);
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}

		return dateString;
	}

	private String formatTime(int hourOfDay, int minute) {
		String timeString = "";
		Calendar now = Calendar.getInstance();
		Calendar then = Calendar.getInstance();

		timeString = hourOfDay + ":" + minute;

		return timeString;
	}

	private boolean validateInput(String name, String location, Date startDate, Date endDate) {
		boolean valid = true;

		if (name.length() == 0) {
			mConferenceNameField.setError("Conference name cannot be empty");
			valid = false;
		} else {

		}

		if (location.length() == 0) {
			mConferenceLocationField.setError("Conference location cannot be empty");
			valid = false;
		} else {

		}

		if (startDate == null) {
			mStartDateField.setError("Conference date cannot be empty");
			valid = false;
		}

		if (endDate == null) {
			mEndDateField.setError("End date cannot be empty");
			valid = false;
		}

		return valid;
	}

	private void saveConference(String name, String location, Date startDate, Date endDate) {
		Conference conference = new Conference();
		conference.setName(name);
		conference.setLocation(location);
		conference.setConferenceDate(startDate);
		conference.setCreator(DocConApplication.getSharedInstance().getAdmin());
		conference.save();

		finish();
	}

}
