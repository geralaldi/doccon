package arnanto.geraldi.doccon;

import com.orm.SugarApp;

import arnanto.geraldi.doccon.helper.SessionManager;
import arnanto.geraldi.doccon.model.Admin;
import arnanto.geraldi.doccon.model.Doctor;

/**
 * Created by geraldi on 10/19/15.
 */
public class DocConApplication extends SugarApp {
	private static DocConApplication instance;
	private SessionManager mSessionManager;
	private Doctor doctor;
	private Admin admin;

	@Override
	public void onCreate() {
		super.onCreate();
		instance = this;
	}

	public static DocConApplication getSharedInstance() {
		return instance;
	}

	public SessionManager getSessionManager() {
		if (mSessionManager == null) {
			mSessionManager = new SessionManager(this);
		}

		return mSessionManager;
	}

	public Doctor getDoctor() {
		if (doctor == null) {
			doctor = Doctor.find(Doctor.class, "email = ?", mSessionManager.getDoctorSession()).get(0);
		}
		return doctor;
	}

	public Admin getAdmin() {
		if (admin == null) {
			admin = Admin.find(Admin.class, "email = ?", mSessionManager.getAdminSession()).get(0);
		}
		return admin;
	}
}
