package arnanto.geraldi.doccon.adapter;

import android.content.Context;
import android.support.v7.widget.AppCompatButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.List;

import arnanto.geraldi.doccon.R;
import arnanto.geraldi.doccon.model.Attendance;
import arnanto.geraldi.doccon.model.Conference;

/**
 * Created by geraldi on 10/22/15.
 */
public class InvitationAdapter extends BaseAdapter {
	private List<Attendance> invitationList;
	private LayoutInflater inflater;

	public InvitationAdapter(Context context, List<Attendance> invitationList) {
		this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.invitationList = invitationList;
	}

	@Override
	public int getCount() {

		// For Showcase
		return 5;
	}

	@Override
	public Attendance getItem(int i) {
		return invitationList.get(i);
	}

	@Override
	public long getItemId(int i) {
		return i;
	}

	@Override
	public View getView(int i, View view, ViewGroup viewGroup) {
		View v = view;
		TextView conferenceNameLabel, conferenceDateLabel, inviterLabel;
		AppCompatButton acceptButton, rejectButton;

//		Attendance attendance = getItem(i);

		if (v == null) {
			v = inflater.inflate(R.layout.item_list_invitation, viewGroup, false);
		}

		// For Showcase
//		conferenceDateLabel = (TextView) v.findViewById(R.id.label_conference_date);
//		conferenceDateLabel.setText(new SimpleDateFormat("E, MMM dd yyyy").format(attendance.getConference()
//				.getConferenceDate()));
//
//		conferenceNameLabel = (TextView) v.findViewById(R.id.label_conference_name);
//		conferenceNameLabel.setText(attendance.getConference().getName());
//
//		inviterLabel = (TextView) v.findViewById(R.id.label_conference_inviter);
//		inviterLabel.setText(attendance.getConference().getCreator().getName());

		acceptButton = (AppCompatButton) v.findViewById(R.id.btn_accept_invitation);
		acceptButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {

			}
		});

		rejectButton = (AppCompatButton) v.findViewById(R.id.btn_reject_invitation);
		rejectButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {

			}
		});

		return v;
	}
}
