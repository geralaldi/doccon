package arnanto.geraldi.doccon.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import arnanto.geraldi.doccon.R;
import arnanto.geraldi.doccon.model.Topic;

/**
 * Created by geraldi on 10/22/15.
 */
public class TopicAdapter extends BaseAdapter {
	private List<Topic> topicList;
	private LayoutInflater inflater;

	public TopicAdapter(Context context, List<Topic> topicList) {
		this.topicList = topicList;
		this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		return topicList.size();
	}

	@Override
	public Topic getItem(int i) {
		return topicList.get(i);
	}

	@Override
	public long getItemId(int i) {
		return i;
	}

	@Override
	public View getView(int i, View view, ViewGroup viewGroup) {
		View v = view;
		ImageView topicImageView;
		TextView topicTitleLabel;
		TextView topicCreatorLabel;
		Topic topic = getItem(i);

		if (v == null) {
			v = inflater.inflate(R.layout.item_list_topic, viewGroup, false);
		}

		topicImageView = (ImageView) v.findViewById(R.id.imageview_topic);
		topicTitleLabel = (TextView) v.findViewById(R.id.label_topic);
		topicCreatorLabel = (TextView) v.findViewById(R.id.label_topic_creator);

		topicTitleLabel.setText(topic.getTitle());
		topicCreatorLabel.setText("by " + topic.getDoctor().getName());

		return v;
	}

	@Override
	public void notifyDataSetChanged() {
		topicList = Topic.listAll(Topic.class);
		super.notifyDataSetChanged();
	}
}
