package arnanto.geraldi.doccon.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.List;

import arnanto.geraldi.doccon.R;
import arnanto.geraldi.doccon.model.Conference;

/**
 * Created by geraldi on 10/22/15.
 */
public class ConferenceAdapter extends BaseAdapter {
	private List<Conference> conferenceList;
	private LayoutInflater inflater;

	public ConferenceAdapter(Context context, List<Conference> conferenceList) {
		this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.conferenceList = conferenceList;
	}

	@Override
	public int getCount() {
		return conferenceList.size();
	}

	@Override
	public Conference getItem(int i) {
		return conferenceList.get(i);
	}

	@Override
	public long getItemId(int i) {
		return i;
	}

	@Override
	public View getView(int i, View view, ViewGroup viewGroup) {
		View v = view;
		TextView conferenceNameLabel, conferenceDateLabel, inviterLabel;

		Conference conference = getItem(i);

		if (v == null) {
			v = inflater.inflate(R.layout.item_list_conference, viewGroup, false);
		}

		// TODO : Implement real data
		conferenceDateLabel = (TextView) v.findViewById(R.id.label_conference_date);
		conferenceDateLabel.setText(new SimpleDateFormat("E, MMM dd yyyy").format(conference.getConferenceDate()));

		conferenceNameLabel = (TextView) v.findViewById(R.id.label_conference_name);
		conferenceNameLabel.setText(conference.getName());

		inviterLabel = (TextView) v.findViewById(R.id.label_conference_inviter);
		inviterLabel.setText("by " + conference.getCreator().getName());

		return v;
	}
}
