package arnanto.geraldi.doccon.model;

import com.orm.SugarRecord;

/**
 * Created by geraldi on 10/22/15.
 */
public class Topic extends SugarRecord {
	String title;
	String image;

	Doctor doctor;

	public Doctor getDoctor() {
		return doctor;
	}

	public void setDoctor(Doctor doctor) {
		this.doctor = doctor;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}
}
