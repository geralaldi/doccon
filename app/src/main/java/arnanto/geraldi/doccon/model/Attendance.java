package arnanto.geraldi.doccon.model;

import com.orm.SugarRecord;

import java.util.Date;

/**
 * Created by geraldi on 10/19/15.
 */
public class Attendance extends SugarRecord {
	Conference conference;
	Admin admin;
	Date dateCreated;
	boolean isConfirmed;

	public Conference getConference() {
		return conference;
	}

	public void setConference(Conference conference) {
		this.conference = conference;
	}

	public Admin getAdmin() {
		return admin;
	}

	public void setAdmin(Admin admin) {
		this.admin = admin;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public boolean isConfirmed() {
		return isConfirmed;
	}

	public void setIsConfirmed(boolean isConfirmed) {
		this.isConfirmed = isConfirmed;
	}
}
