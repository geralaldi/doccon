package arnanto.geraldi.doccon.model;

import com.orm.SugarRecord;

import java.util.Date;

/**
 * Created by geraldi on 10/19/15.
 */
public class Doctor extends SugarRecord {
	int id;
	String name;
	String email;
	String password;
	Date dateCreated;
	Date dateUpdated;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Date getDateUpdated() {
		return dateUpdated;
	}

	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}
}
