package arnanto.geraldi.doccon;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;

import java.util.List;

import arnanto.geraldi.doccon.helper.SessionManager;
import arnanto.geraldi.doccon.model.Doctor;

public class LoginActivity extends AppCompatActivity {

	EditText mEmailField;
	EditText mPasswordField;
	TextView mRegisterLabel;
	AppCompatButton mLoginButton;
	Toolbar mActionBar;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);

		setupView();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_login, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();

		//noinspection SimplifiableIfStatement
		if (id == R.id.action_settings) {
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	private void setupView() {
		mActionBar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(mActionBar);

		mEmailField = (EditText) findViewById(R.id.field_email);
		mPasswordField = (EditText) findViewById(R.id.field_password);

		mLoginButton = (AppCompatButton) findViewById(R.id.btn_login);
		mLoginButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				String email = mEmailField.getText().toString();
				String password = mPasswordField.getText().toString();

				if(validateInput(email, password)) {
					// Check credential on database
					List<Doctor> doctors = Doctor.find(Doctor.class, "email = ?", email);
					if (!doctors.isEmpty()) {
						Doctor doctor = doctors.get(0);
						if (doctor.getPassword().equals(password)) {
							saveLogin(doctor);

							// Go to main activity
							Intent i = new Intent(LoginActivity.this, MainDoctorActivity.class);
							startActivity(i);
						} else {
							// Popup login error
							Snackbar.make(mLoginButton, "Username or password is not correct", Snackbar.LENGTH_LONG).show
									();
						}
					} else {
						// Popup login error
						Snackbar.make(mLoginButton, "Username or password is not correct", Snackbar.LENGTH_LONG).show
								();
					}
				}
			}
		});

		mRegisterLabel = (TextView) findViewById(R.id.link_signup);
		mRegisterLabel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent i = new Intent(LoginActivity.this, RegistrationActivity.class);
				startActivity(i);
			}
		});
	}

	private boolean validateInput(String email, String password) {
		boolean valid = true;

		if (email.length() == 0) {
			mEmailField.setError("Please input your username");
			valid = false;
		} else {
			// Check for username validity
		}

		if (password.length() == 0) {
			mPasswordField.setError("Please input your account's password");
			valid = false;
		} else {
			// Check for password validity
		}

		return valid;
	}

	private void saveLogin(Doctor doctor) {
		SessionManager sessionManager = DocConApplication.getSharedInstance().getSessionManager();
		sessionManager.createDoctorSession(doctor);
	}
}
