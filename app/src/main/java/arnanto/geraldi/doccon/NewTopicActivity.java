package arnanto.geraldi.doccon;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;

import arnanto.geraldi.doccon.model.Doctor;
import arnanto.geraldi.doccon.model.Topic;

public class NewTopicActivity extends AppCompatActivity {

	EditText mNewTopicField;
	AppCompatButton mCreateTopicButton;
	Toolbar mToolbar;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_new_topic);
		mToolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(mToolbar);

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		setupView();

		setTitle("Create New Topic");
	}

	@Override
	public void setTitle(CharSequence title) {
		super.setTitle(title);

		mToolbar.setTitle(title);
	}

	private void setupView() {
		mNewTopicField = (EditText) findViewById(R.id.field_topic_name);

		mCreateTopicButton = (AppCompatButton) findViewById(R.id.btn_create_topic);
		mCreateTopicButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				String topicName = mNewTopicField.getText().toString();

				if (validateInput(topicName)) {
					saveTopic(topicName);
				}
			}
		});
	}

	private boolean validateInput(String name) {
		boolean valid = true;

		if (name.length() == 0) {
			mNewTopicField.setError("Topic name cannot be empty");
			valid = false;
		} else {

		}

		return valid;
	}

	private void saveTopic(String name) {
		Doctor doctor = DocConApplication.getSharedInstance().getDoctor();

		Topic topic = new Topic();
		topic.setTitle(name);
		topic.setDoctor(doctor);
		topic.save();

		finish();
	}

}
