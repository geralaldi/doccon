package arnanto.geraldi.doccon.helper;

import android.content.Context;
import android.content.SharedPreferences;

import arnanto.geraldi.doccon.model.Admin;
import arnanto.geraldi.doccon.model.Doctor;

/**
 * Created by geraldi on 10/22/15.
 */
public class SessionManager {

	private static final String PREF_NAME = SessionManager.class.getSimpleName();
	private static final String PREF_KEY_DOCTOR_EMAIL = "doctor_email";
	private static final String PREF_KEY_DOCTOR_NAME = "doctor_name";
	private static final String PREF_KEY_ADMIN_EMAIL = "admin_email";
	private static final String PREF_KEY_ADMIN_NAME = "admin_name";
	private static final String PREF_KEY_LOGGED_IN = "logged_in";

	private Context context;
	private SharedPreferences prefs;
	private SharedPreferences.Editor editor;

	public SessionManager(Context context) {
		this.context = context;
		prefs = this.context.getSharedPreferences(PREF_NAME, 0);
		editor = prefs.edit();
	}

	public void createDoctorSession(Doctor doctor) {
		editor.putString(PREF_KEY_DOCTOR_EMAIL, doctor.getEmail());
		editor.putString(PREF_KEY_DOCTOR_NAME, doctor.getName());
		editor.putBoolean(PREF_KEY_LOGGED_IN, true);
		editor.commit();
	}

	public String getDoctorSession() {
		return prefs.getString(PREF_KEY_DOCTOR_EMAIL, null);
	}

	public void createAdminSession(Admin admin) {
		editor.putString(PREF_KEY_ADMIN_EMAIL, admin.getEmail());
		editor.putString(PREF_KEY_ADMIN_NAME, admin.getName());
		editor.putBoolean(PREF_KEY_LOGGED_IN, true);
		editor.commit();
	}

	public String getAdminSession() {
		return prefs.getString(PREF_KEY_ADMIN_EMAIL, null);
	}

	public void logoutUser() {
		editor.clear();
		editor.commit();
	}

	public boolean isUserLoggedIn() {
		return prefs.getBoolean(PREF_KEY_LOGGED_IN, false);
	}

}
