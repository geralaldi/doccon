package arnanto.geraldi.doccon.fragment;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import java.util.List;

import arnanto.geraldi.doccon.DocConApplication;
import arnanto.geraldi.doccon.MainAdminActivity;
import arnanto.geraldi.doccon.R;
import arnanto.geraldi.doccon.RegistrationActivity;
import arnanto.geraldi.doccon.helper.SessionManager;
import arnanto.geraldi.doccon.model.Admin;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link AdminLoginFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link AdminLoginFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AdminLoginFragment extends Fragment {
	// TODO: Rename parameter arguments, choose names that match
	// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
	private static final String ARG_PARAM1 = "param1";
	private static final String ARG_PARAM2 = "param2";

	// TODO: Rename and change types of parameters
	private String mParam1;
	private String mParam2;

	private OnFragmentInteractionListener mListener;

	EditText mEmailField;
	EditText mPasswordField;
	TextView mRegisterLabel;
	AppCompatButton mLoginButton;

	/**
	 * Use this factory method to create a new instance of
	 * this fragment using the provided parameters.
	 *
	 * @param param1 Parameter 1.
	 * @param param2 Parameter 2.
	 * @return A new instance of fragment AdminLoginFragment.
	 */
	// TODO: Rename and change types and number of parameters
	public static AdminLoginFragment newInstance(String param1, String param2) {
		AdminLoginFragment fragment = new AdminLoginFragment();
		Bundle args = new Bundle();
		args.putString(ARG_PARAM1, param1);
		args.putString(ARG_PARAM2, param2);
		fragment.setArguments(args);
		return fragment;
	}

	public AdminLoginFragment() {
		// Required empty public constructor
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (getArguments() != null) {
			mParam1 = getArguments().getString(ARG_PARAM1);
			mParam2 = getArguments().getString(ARG_PARAM2);
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_admin_login, container, false);
		setupView(view);
		return view;
	}

	// TODO: Rename method, update argument and hook method into UI event
	public void onButtonPressed(Uri uri) {
		if (mListener != null) {
			mListener.onFragmentInteraction(uri);
		}
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			mListener = (OnFragmentInteractionListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement OnFragmentInteractionListener");
		}
	}

	@Override
	public void onDetach() {
		super.onDetach();
		mListener = null;
	}

	private void setupView(View view) {
		mEmailField = (EditText) view.findViewById(R.id.field_email);
		mPasswordField = (EditText) view.findViewById(R.id.field_password);

		mLoginButton = (AppCompatButton) view.findViewById(R.id.btn_login);
		mLoginButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				String email = mEmailField.getText().toString();
				String password = mPasswordField.getText().toString();

				if(validateInput(email, password)) {
					// Check credential on database
					List<Admin> admins = Admin.find(Admin.class, "email = ?", email);
					if (!admins.isEmpty()) {
						Admin admin = admins.get(0);
						if (admin.getPassword().equals(password)) {
							saveLogin(admin);

							// Go to main activity
							Intent i = new Intent(getActivity(), MainAdminActivity.class);
							startActivity(i);
						} else {
							// Popup login error
							Snackbar.make(mLoginButton, "Username or password is not correct", Snackbar.LENGTH_LONG).show
									();
						}
					} else {
						// Popup login error
						Snackbar.make(mLoginButton, "Username or password is not correct", Snackbar.LENGTH_LONG).show
								();
					}
				}
			}
		});

		mRegisterLabel = (TextView) view.findViewById(R.id.link_signup);
		mRegisterLabel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent i = new Intent(getActivity(), RegistrationActivity.class);
				i.putExtra(RegistrationActivity.EXTRA_KEY_ACCOUNT_TYPE, RegistrationActivity.ACCOUNT_TYPE_ADMIN);
				startActivity(i);
			}
		});
	}

	private boolean validateInput(String email, String password) {
		boolean valid = true;

		if (email.length() == 0) {
			mEmailField.setError("Please input your username");
			valid = false;
		} else {
			// Check for username validity
		}

		if (password.length() == 0) {
			mPasswordField.setError("Please input your account's password");
			valid = false;
		} else {
			// Check for password validity
		}

		return valid;
	}

	private void saveLogin(Admin admin) {
		SessionManager sessionManager = DocConApplication.getSharedInstance().getSessionManager();
		sessionManager.createAdminSession(admin);
	}

	/**
	 * This interface must be implemented by activities that contain this
	 * fragment to allow an interaction in this fragment to be communicated
	 * to the activity and potentially other fragments contained in that
	 * activity.
	 * <p/>
	 * See the Android Training lesson <a href=
	 * "http://developer.android.com/training/basics/fragments/communicating.html"
	 * >Communicating with Other Fragments</a> for more information.
	 */
	public interface OnFragmentInteractionListener {
		// TODO: Update argument type and name
		public void onFragmentInteraction(Uri uri);
	}

}
