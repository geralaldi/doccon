package arnanto.geraldi.doccon;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;

import arnanto.geraldi.doccon.model.Admin;
import arnanto.geraldi.doccon.model.Doctor;

public class RegistrationActivity extends AppCompatActivity {

	public static final String EXTRA_KEY_ACCOUNT_TYPE = "account_type";
	public static final int ACCOUNT_TYPE_DOCTOR = 0;
	public static final int ACCOUNT_TYPE_ADMIN = 1;

	EditText mNameField;
	EditText mEmailField;
	EditText mPasswordField;
	AppCompatButton mRegisterButton;

	int accountType;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_registration);

		if (getIntent().getExtras() != null) {
			accountType = getIntent().getExtras().getInt(EXTRA_KEY_ACCOUNT_TYPE);
		}

		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		setupView();
	}

	private void setupView() {
		mNameField = (EditText) findViewById(R.id.field_name);
		mEmailField = (EditText) findViewById(R.id.field_email);
		mPasswordField = (EditText) findViewById(R.id.field_password);

		mRegisterButton = (AppCompatButton) findViewById(R.id.btn_signup);
		mRegisterButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				String name = mNameField.getText().toString();
				String email = mEmailField.getText().toString();
				String password = mPasswordField.getText().toString();

				if (validateInput(name, email, password)) {
					registerUser(name, email, password);
				}
			}
		});
	}

	private boolean validateInput(String name, String email, String password) {
		boolean valid = true;

		if (name.length() == 0) {
			mNameField.setError("Please input your name");
			valid = false;
		} else {
			// Check for name validity
		}

		if (email.length() == 0) {
			mEmailField.setError("Please input a valid email");
			valid = false;
		} else {
			// Check for email validity
		}

		if (password.length() == 0) {
			mPasswordField.setError("Please input your account's password");
			valid = false;
		} else {
			// Check for password validity
		}

		return valid;
	}

	private void registerUser(String name, String email, String password) {
		switch (accountType) {
			case ACCOUNT_TYPE_ADMIN:
				Admin admin = new Admin();
				admin.setName(name);
				admin.setEmail(email);
				admin.setPassword(password);
				admin.save();
				break;
			case ACCOUNT_TYPE_DOCTOR:
				Doctor doctor = new Doctor();
				doctor.setName(name);
				doctor.setEmail(email);
				doctor.setPassword(password);
				doctor.save();
				break;
			default:
				break;
		}

		dismissRegistration();
	}

	private void dismissRegistration() {
		// TODO : Show Registration Successful Alert
		finish();
	}

}
