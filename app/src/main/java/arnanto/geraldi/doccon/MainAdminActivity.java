package arnanto.geraldi.doccon;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import arnanto.geraldi.doccon.fragment.ConferenceFragment;
import arnanto.geraldi.doccon.fragment.TopicFragment;
import arnanto.geraldi.doccon.model.Admin;

public class MainAdminActivity extends AppCompatActivity
		implements NavigationView.OnNavigationItemSelectedListener, TopicFragment.OnFragmentInteractionListener, ConferenceFragment.OnFragmentInteractionListener {

	TextView mUserNameLabel;
	TextView mUserEmailLabel;
	ImageView mUserImageView;
	Toolbar mToolbar;

	Admin admin;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main_admin);

		mToolbar = (Toolbar) findViewById(R.id.toolbar);
		mToolbar.setTitle("DocCon");
		setSupportActionBar(mToolbar);

		DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
		ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
				this, drawer, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
		drawer.setDrawerListener(toggle);
		toggle.syncState();

		NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
		navigationView.setNavigationItemSelectedListener(this);

		setupNavigationView(navigationView);

		FragmentManager fragmentManager = getFragmentManager();
		fragmentManager.beginTransaction().replace(R.id.layout_container, ConferenceFragment.newInstance(null, null))
				.commit();
		setTitle("Conferences");
	}

	@Override
	public void onBackPressed() {
		DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
		if (drawer.isDrawerOpen(GravityCompat.START)) {
			drawer.closeDrawer(GravityCompat.START);
		} else {
			super.onBackPressed();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main_admin, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();

		//noinspection SimplifiableIfStatement
		if (id == R.id.action_settings) {
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	@SuppressWarnings("StatementWithEmptyBody")
	@Override
	public boolean onNavigationItemSelected(MenuItem item) {
		// Handle navigation view item clicks here.
		int id = item.getItemId();

		Fragment fragment = null;
		Class fragmentClass = null;
		if (id == R.id.nav_conferences) {
			fragmentClass = ConferenceFragment.class;

			fragment = ConferenceFragment.newInstance(null, null);
			setTitle("Conferences");
		} else if (id == R.id.nav_conference_topic) {
			fragmentClass = TopicFragment.class;

			fragment = TopicFragment.newInstance("Admin", null);
			setTitle("Topics");
		} else if (id == R.id.nav_admin_logout) {
			DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
			drawer.closeDrawer(GravityCompat.START);

			DocConApplication.getSharedInstance().getSessionManager().logoutUser();

			Intent i = new Intent(this, MainLoginActivity.class);
			i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
			startActivity(i);

			return true;
		}

		FragmentManager fragmentManager = getFragmentManager();
		fragmentManager.beginTransaction().replace(R.id.layout_container, fragment).commit();

		DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
		drawer.closeDrawer(GravityCompat.START);
		return true;
	}

	@Override
	public void setTitle(CharSequence title) {
		super.setTitle(title);

		mToolbar.setTitle(title);
	}

	private void setupNavigationView(NavigationView navigationView) {
		LinearLayout headerLayout = (LinearLayout) getLayoutInflater().inflate(R.layout.nav_header_main_admin, navigationView, false);

		navigationView.addHeaderView(headerLayout);

		mUserNameLabel = (TextView) headerLayout.findViewById(R.id.label_user_name);
		mUserEmailLabel = (TextView) headerLayout.findViewById(R.id.label_user_email);

		mUserImageView = (ImageView) headerLayout.findViewById(R.id.image_user);

		admin = DocConApplication.getSharedInstance().getAdmin();

		mUserNameLabel.setText(admin.getName());
		mUserEmailLabel.setText(admin.getEmail());
	}

	@Override
	public void onFragmentInteraction(String id) {

	}
}
